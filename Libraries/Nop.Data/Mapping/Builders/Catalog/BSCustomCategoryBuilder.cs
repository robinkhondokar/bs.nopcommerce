﻿using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Builders.Catalog
{
    public partial class BSCustomCategoryBuilder : NopEntityBuilder<BSCustomCategory>
    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(BSCustomCategory.Name)).AsString(400).NotNullable()
                .WithColumn(nameof(BSCustomCategory.MetaKeywords)).AsString(400).Nullable()
                .WithColumn(nameof(BSCustomCategory.MetaTitle)).AsString(400).Nullable();
        }
    }
}
