﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Migrations
{
    [NopMigration("2022/04/22 12:00:00:2551770","4.40", UpdateMigrationType.Data)]
    public class AddBSCustomFeatureProperty: AutoReversingMigration
    {
        public override void Up()
        {
            Create.Column(nameof(Category.BSCustomFeature))
                .OnTable("Category")
                .AsString(255)
                .Nullable();
        }
    }
}
